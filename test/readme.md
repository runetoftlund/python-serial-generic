tests
===========

The script `fake_device_conn.py` is a simple echo script, that will make some pre-defined answer to what it receives.
The script `fake_device_send.py` will send a value every second.


Both will connect to a serial tty using 9600 baud.

In order to use it for testing, do the following (in the `test` directory)

Create two virtual serial port and connect them
```
socat -d -d pty,raw,echo=0,link=./fake_device pty,raw,echo=0,link=client
```

Start the fake serial server.
```
./fake_device_conn.py
```

and connect to it using e.g. `minicom` or your own client in a new terminal
```
minicom -D ./client -b 9600
```
